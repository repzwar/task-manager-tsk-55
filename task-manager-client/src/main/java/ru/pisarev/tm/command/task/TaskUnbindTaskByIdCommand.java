package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class TaskUnbindTaskByIdCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-unbind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskDto task = taskEndpoint.findTaskById(getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        taskEndpoint.unbindTaskById(getSession(), taskId);
    }

}
