package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class TaskFinishByNameCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final TaskDto task = taskEndpoint.finishTaskByName(getSession(), name);
        if (task == null) throw new TaskNotFoundException();
    }

}
