package ru.pisarev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pisarev.tm.component.Bootstrap;
import ru.pisarev.tm.endpoint.SessionDto;
import ru.pisarev.tm.exception.system.AccessDeniedException;

public abstract class AbstractCommand {

    @Nullable
    @Autowired
    protected Bootstrap sessionLocator;

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String arg = arg();
        @Nullable final String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

    @Nullable
    protected SessionDto getSession() {
        @Nullable SessionDto session = sessionLocator.getSession();
        if (session == null) throw new AccessDeniedException();
        return session;
    }

}
