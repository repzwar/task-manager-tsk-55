package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.endpoint.DataEndpoint;

@Component
public class DataYamlLoadFasterXMLCommand extends AuthAbstractCommand {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-load-yaml";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Load data from JSON by FasterXML.";
    }

    public void execute() {
        dataEndpoint.loadDataYaml(getSession());
    }

}