package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.pisarev.tm.api.service.dto.ISessionRecordService;
import ru.pisarev.tm.api.service.dto.IUserRecordService;
import ru.pisarev.tm.api.service.model.ISessionService;
import ru.pisarev.tm.dto.SessionRecord;
import ru.pisarev.tm.dto.UserRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @NotNull
    @Autowired
    private IUserRecordService userService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @WebMethod
    public SessionRecord open(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {

        SessionRecord session = sessionRecordService.open(login, password);
        return session;
    }

    @WebMethod
    public void close(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session);
        sessionRecordService.close(session);
    }

    @WebMethod
    public SessionRecord register(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        userService.add(login, password, email);
        return sessionRecordService.open(login, password);
    }

    @WebMethod
    public UserRecord setPassword(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "password") final String password
    ) {
        sessionRecordService.validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public UserRecord updateUser(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        sessionRecordService.validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
