package ru.pisarev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.pisarev.tm.api.service.dto.ISessionRecordService;
import ru.pisarev.tm.api.service.dto.IUserRecordService;
import ru.pisarev.tm.api.service.model.IUserService;
import ru.pisarev.tm.dto.SessionRecord;
import ru.pisarev.tm.dto.UserRecord;
import ru.pisarev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private IUserRecordService userRecordService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ISessionRecordService sessionService;

    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "login") final String login
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @WebMethod
    public UserRecord lockByLogin(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "login") final String login
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        return userRecordService.lockByLogin(login);
    }

    @WebMethod
    public UserRecord unlockByLogin(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "login") final String login
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        return userRecordService.unlockByLogin(login);
    }

    @WebMethod
    public void closeAllByUserId(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "userId") final String userId
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        sessionService.closeAllByUserId(userId);
    }

    @Nullable
    @WebMethod
    public List<SessionRecord> findAllByUserId(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "userId") final String userId
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        return sessionService.findAllByUserId(userId);
    }
}
