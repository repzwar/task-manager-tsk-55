package ru.pisarev.tm.api;

import ru.pisarev.tm.dto.AbstractRecord;

import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

public interface IRecordRepository<E extends AbstractRecord> {

    List<E> findAll();

    void add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void close();

    EntityTransaction getTransaction();

}