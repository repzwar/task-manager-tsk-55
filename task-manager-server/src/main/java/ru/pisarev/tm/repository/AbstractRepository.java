package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class AbstractRepository {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected EntityManager entityManager;

    public AbstractRepository() {
        entityManager = context.getBean(EntityManager.class);
    }

    public void close() {
        entityManager.close();
    }

    public EntityTransaction getTransaction() {
        return entityManager.getTransaction();
    }
}
